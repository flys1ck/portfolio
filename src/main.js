import { createApp } from "vue";
import App from "./App.vue";

// Tailwind CSS
import "./assets/styles/tailwind.css";

createApp(App).mount("#app");
