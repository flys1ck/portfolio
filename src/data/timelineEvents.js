const timelineEvents = [
  {
    employer: {
      name: "Funkwerk",
      link: "https://funkwerk.com",
      location: "Kölleda, Germany"
    },
    position: "Dual Student",
    startTime: "Oktober 2015",
    endTime: "August 2018",
    bullets: [
      "Applying techniques of functional safety norms",
      "Development of tools for tool-assisted auditing of software componentes",
      "Development of an web application for automatic requirement monitoring and assignment using Angular"
    ]
  },
  {
    employer: {
      name: "ePages",
      link: "https://epages.com",
      location: "Jena, Germany"
    },
    position: "Working Student",
    startTime: "February 2019",
    endTime: "Present",
    bullets: [
      "Development of a pre- and postprocessing toolchain for continuous software localization",
      "Working in a team environment developing features and maintaing the companies eCommerce Plattform"
    ]
  }
];

export default timelineEvents;
