const projects = [
  {
    title: "Sample Generation",
    release: "February 2020",
    tags: ["Python", "Cython", "PyTest"],
    brief: "Fast sample generation for probabilistic programming",
    description:
      "This project was part of the university module Algorithm Engineering (Lab).",
    repository: {
      hostname: "GitLab",
      link: "https://gitlab.com/flys1ck/graphical-models-sampling"
    }
  },
  {
    title: "Developer Portfolio",
    release: "June 2020",
    tags: ["Vue", "TailwindCSS", "Now"],
    brief: "Self-designed and developed personal website",
    description:
      "This project was part of the university module Algorithm Engineering (Lab).",
    repository: {
      hostname: "GitLab",
      link: "https://gitlab.com/flys1ck/portfolio"
    }
  }
];
export default projects;
