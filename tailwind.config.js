module.exports = {
  theme: {
    extend: {
      fontFamily: {
        body: ["Open Sans", "sans-serif"],
        body2: ["Balsamiq Sans", "sans-serif"],
        body3: ["Lato", "sans-serif"],
        body4: ["Montserrat", "sans-serif"],
        head: ["Bungee", "sans-serif"]
      },
      height: {
        "0.5": "0.125rem"
      },
      width: {
        "0.5": "0.125rem"
      },
      fontSize: {
        "7xl": "5rem",
        "8xl": "6rem",
        "9xl": "7rem",
        "10xl": "8rem"
      }
    }
  },
  variants: {},
  plugins: []
};
