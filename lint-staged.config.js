module.exports = {
  "*.js": "eslint --cache --fix",
  "*.css": "stylelint --fix",
  "*.vue": "vue-cli-service lint"
};
