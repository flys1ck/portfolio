module.exports = {
  hooks: {
    "pre-commit": "lint-staged && git add",
    "commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
  }
};
