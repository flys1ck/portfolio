# Contributing

Please be aware of the following guidelines, when working and contributing to this project.

## Commit Message Guidelines

The commit message guidelines follow [Conventional Commits](https://www.conventionalcommits.org/) and the sample of the [Angular Commit Message Guidelines](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines).

### Commit Message Format

Each commit message consists of a header and a body. The header has a special format that includes a type and a subject:

```
<type>: <subject>

<body>
```

The header is mandatory and the body is optional. Any line of the commit message should not be longer than 100 characters!

### Type

Must be one of the following:

* **build**: Changes that affect the build system, deploy system or external dependencies
* **docs**: Documentation only changes
* **feat**: A new feature
* **fix**: A bug fix
* **refactor**: A code change that neither fixes a bug nor adds a feature

### Subject

The subject contains a succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize the first letter
* no dot (.) at the end

### Body

Just as in the subject, use the imperative, present tense: "change" not "changed" nor "changes". The body should include the motivation for the change and contrast this with previous behavior.

### Samples

```
docs: update changelog to beta.5
```

```
fix: need to depend on latest rxjs and zone.js

The version in our package.json gets copied to the one we publish, and users need the latest of these.
```
